package com.example.exitanim;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private ImageView iv_line;
	private Animation mExitAnim;
	private long mLastClickBackTime = 0;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		initExitAnim();
	}

	private void initExitAnim() {
		iv_line = (ImageView) this.findViewById(R.id.iv_off);
		mExitAnim = AnimationUtils.loadAnimation(getApplicationContext(),
				R.anim.tv_off);
		mExitAnim.setAnimationListener(new AnimationListener() {
			@Override
			public void onAnimationStart(Animation animation) {
			}

			@Override
			public void onAnimationRepeat(Animation animation) {
			}

			@Override
			public void onAnimationEnd(Animation animation) {
				MainActivity.this.finish();
			}
		});
	}

	// 返回键处理
	@Override
	public void onBackPressed() {
		long now = System.currentTimeMillis();
		if (mLastClickBackTime != 0
				&& Math.abs(now - mLastClickBackTime) < 2000) {
			iv_line.setVisibility(View.VISIBLE);
			iv_line.startAnimation(mExitAnim);
		} else {
			Toast.makeText(this, "再按一次退出程序", Toast.LENGTH_SHORT).show();
			mLastClickBackTime = now;
			return;
		}
	}

}
